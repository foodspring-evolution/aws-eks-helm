import os

from bitbucket_pipes_toolkit.test import PipeTestCase

CLUSTER_NAME = f"bbci-test-eks-{os.getenv('BITBUCKET_BUILD_NUMBER')}"
AWS_DEFAULT_REGION = "us-east-1"


class EKSTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_list(self):
        result = self.run_container(
            environment={
                "HELM_COMMAND": "list",
                "CLUSTER_NAME": CLUSTER_NAME,
                "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
                "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
                "AWS_DEFAULT_REGION": AWS_DEFAULT_REGION,
                "DEBUG": "true",
            }
        )

        self.assertIn("helm list was successful", result)

    def test_apply_fails_when_cluster_doesnt_exist(self):
        result = self.run_container(
            environment={
                "HELM_COMMAND": "list",
                "CLUSTER_NAME": "no-such-cluster",
                "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
                "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
                "AWS_DEFAULT_REGION": AWS_DEFAULT_REGION,
            }
        )

        self.assertIn("No cluster found for name: no-such-cluster", result)
