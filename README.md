# Bitbucket Pipelines Pipe: AWS EKS run command

Run a command against a AWS EKS cluster. This pipe uses [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/), a command line interface for running commands against Kubernetes clusters.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: foodspring/aws-eks-helm:1.0.0
  variables:
    AWS_ACCESS_KEY_ID: '<string>' # Optional if already defined in the context.
    AWS_SECRET_ACCESS_KEY: '<string>' # Optional if already defined in the context.
    AWS_DEFAULT_REGION: '<string>' # Optional if already defined in the context.
    CLUSTER_NAME: '<string>'
    HELM_COMMAND: '<string>'
    # LABELS: '<array>' # Optional
    # WITH_DEFAULT_LABELS: '<boolean>' # Optional
    # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (**) | AWS access key id.  |
| AWS_SECRET_ACCESS_KEY (**) | AWS secret access key. |
| AWS_DEFAULT_REGION (**) | AWS region. |
| CLUSTER_NAME (*)      | The name of a kubernetes cluster. |
| HELM_COMMAND (*)      | Helm command to run.
| LABELS                | Key/value pairs that are attached to objects, such as pods. Labels are intended to be used to specify identifying attributes of objects. |
| WITH_DEFAULT_LABELS   | Whether or not to add the default labels. Check Labels added by default section for more details. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
_(*) = required variable. This variable needs to be specified always when using the pipe._
_(**) = required variable. If this variable is configured as a repository, account or environment variable, it doesn’t need to be declared in the pipe as it will be taken from the context. It can still be overridden when using the pipe._


## EKS Authentication

* When using EKS, you have to remember, that IAM roles don't make any sense inside a EKS cluster, unless you explicitly configure the mapping, as described in [Managing Users or IAM Roles for your Cluster](https://docs.aws.amazon.com/eks/latest/userguide/add-user-role.html).
* It is important to know, that only the IAM user who created the cluster has permissions to access it. Refer to the page linked above for how to grant your IAM users and roles permissions within the EKS cluster. From AWS docs:
  > When you create an Amazon EKS cluster, the IAM entity user or role, such as a federated user that creates the cluster, is automatically granted system:masters permissions in the cluster's RBAC configuration. To grant additional AWS users or roles the ability to interact with your cluster, you must edit the aws-auth ConfigMap within Kubernetes.

## Labels added by default

By default, the pipe will use the following labels in order to track which pipeline created the Kubernetes resources and be able to link it back to

| Label  | Description   |
| ------  | -------------|
| `bitbucket.org/bitbucket_commit` | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f`|
| `bitbucket.org/bitbucket_deployment_environment`| The name of the environment which the step deploys to. This is only available on deployment steps.|
| `bitbucket.org/bitbucket_repo_owner`| The name of the owner account. |
| `bitbucket.org/bitbucket_repo_slug` | Repository name. |
| `bitbucket.org/bitbucket_build_number` | Bitbucket Pipeline number |
| `bitbucket.org/bitbucket_step_triggerer_uuid` | UUID from the user who triggered the step execution. |

## Prerequisites
 - Basic knowledge is required of how Kubernetes works and how to create services and deployments on it.
 - Kubernetes cluster running in EKS is required to use this pipe. Check out this [getting started](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html) guide from AWS.
 - A docker registry (Docker Hub or similar) to store your docker image: if you are deploying to a Kubernetes cluster you will need a docker registry to store you images.

## Examples

### Basic example:

Run command 'apply' against a AWS EKS cluster.

```yaml
script:
  - pipe: foodspring/aws-eks-helm:1.0.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      HELM_COMMAND: 'list'
```

Basic example. `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` and `AWS_DEFAULT_REGION` are configured as repository variables, so there is no need to declare them in the pipe.

```yaml
script:
  - pipe: foodspring/aws-eks-helm:1.0.0
    variables:
      CLUSTER_NAME: 'my-kube-cluster'
      HELM_COMMAND: 'upgrade my-nginx --install stable/nginx'
```

### Advanced example:

Using local helm chart

```yaml
script:
  - pipe: foodspring/aws-eks-helm:1.0.0
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      HELM_COMMAND: 'install my-chart local/path/to/chart_dir/'
```
