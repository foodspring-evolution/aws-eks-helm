import sys
import subprocess
import json
import base64

from kubectl_run.pipe import KubernetesDeployPipe

schema = {
    "AWS_ACCESS_KEY_ID": {"type": "string", "required": True},
    "AWS_SECRET_ACCESS_KEY": {"type": "string", "required": True},
    "AWS_DEFAULT_REGION": {"type": "string", "required": True},
    "CLUSTER_NAME": {"type": "string", "required": True},
    "ROLE_ARN": {"type": "string", "required": False, "nullable": True},
    "HELM_COMMAND": {"type": "string", "required": True},
    "HELM_NAMESPACE": {"type": "string", "required": False},
    "HELM_VALUES_PATH": {"type": "string", "required": False},
    "HELM_VALUES_CONFIG_MAP": {"type": "string", "required": False},
    "HELM_VALUES_CONFIG_MAP_NAMESPACE": {"type": "string", "required": False},
    "HELM_COPY_CONFIG_MAP": {"type": "string", "required": False},
    "HELM_COPY_SECRET": {"type": "string", "required": False},
    "HELM_COPY_NAMESPACE": {"type": "string", "required": False},
    "RESOURCE_PATH": {
        "type": "string",
        "required": False,
        "nullable": True,
        "default": "",
    },
    "LABELS": {"type": "list", "required": False},
    "WITH_DEFAULT_LABELS": {"type": "boolean", "required": False, "default": True},
    "DEBUG": {"type": "boolean", "required": False, "default": False},
}


class EKSHelmPipe(KubernetesDeployPipe):
    def configure(self):
        cluster_name = self.get_variable("CLUSTER_NAME")
        role = self.get_variable("ROLE_ARN")
        cmd = f"aws eks update-kubeconfig --name={cluster_name}".split()
        if role is not None:
            cmd.append(f"--role-arn={role}")
        self.log_info(self.get_variable("DEBUG"))
        if self.get_variable("DEBUG"):
            cmd.append("--verbose")
        result = subprocess.run(cmd, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f"Failed to update the kube config.")
        else:
            self.success(f"Successfully updated the kube config.")

    def handle_deploy(self, cmd):
        debug = ["--debug"] if self.get_variable("DEBUG") else []
        namespace = (
            ["--namespace", self.get_variable("HELM_NAMESPACE")]
            if self.get_variable("HELM_NAMESPACE")
            else []
        )

        configmaps = self.get_variable("HELM_VALUES_CONFIG_MAP")
        if configmaps:
            configmaps = configmaps.split(",")
        values = []
        if configmaps:
            for cm in configmaps:
                with open(cm + "values.yaml", "w") as output:
                    result = subprocess.run(
                        ["kubectl"]
                        + namespace
                        + [
                            "get",
                            "configmap",
                            cm,
                            "-o",
                            "jsonpath={.data.values\\.yaml}",
                        ],
                        stdout=output,
                    )
                    if result.returncode != 0:
                        self.fail(f"kubectl get configmap {cm} failed")
                values += ["--values", cm + "values.yaml"]
        else:
            values_path = self.get_variable("HELM_VALUES_PATH")
            values = ["--values", values_path] if values_path else []

        real_cmd = ["helm"] + namespace + cmd.split() + values + debug
        result = subprocess.run(real_cmd, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f"{' '.join(real_cmd)} failed.")
        else:
            self.success(f"{' '.join(real_cmd)} was successful.")

    def handle_generic(self, cmd):
        debug = ["--debug"] if self.get_variable("DEBUG") else []
        result = subprocess.run(["helm"] + cmd.split() + debug, stdout=sys.stdout)

        if result.returncode != 0:
            self.fail(f"helm {cmd} failed.")
        else:
            self.success(f"helm {cmd} was successful.")

    def copy_configs(self, object_type, object_name):
        db_instance = self.get_variable("HELM_COPY_NAMESPACE")
        ns = self.get_variable("HELM_NAMESPACE") # In our case, db_instance is the same as namespace

        namespace = (
            ["--namespace", self.get_variable("HELM_COPY_NAMESPACE")]
            if self.get_variable("HELM_COPY_NAMESPACE")
            else []
        )
        target_namespace = (
            ["--namespace", self.get_variable("HELM_NAMESPACE")]
            if self.get_variable("HELM_NAMESPACE")
            else []
        )

        target_values = []
        result = subprocess.run(
            ["kubectl"]
            + target_namespace
            + [
                "get",
                object_type,
                object_name,
                "-o",
                "json"
            ],
            stdout=subprocess.PIPE,
        )
        if result.returncode != 0:
            self.fail(f"kubectl get {object_type} {object_name} failed. {result.returncode}")

        decoded = result.stdout.decode('utf8')
        data = json.loads(decoded)

        # remove fields to get structure just as --export used to produce
        del data['metadata']['namespace']
        del data['metadata']['resourceVersion']
        del data['metadata']['uid']
        del data['metadata']['creationTimestamp']

        # special treatment for shop secrets
        if object_type == "secret" and "database_url_slug" in data["data"]:
            # for secret, reconstruct database_url.
            updated_url = base64.b64decode(data["data"]["database_url_slug"])
            updated_url += ("/ephemeral_"+ db_instance).encode("utf-8")
            data["data"]["database_url"] = base64.b64encode(updated_url).decode("utf-8")


        # dump file
        cm_values = ["-f", "cm_values.json"]
        with open('cm_values.json', 'w') as outfile:
            json.dump(data, outfile)

        apply_cmd = ["kubectl"] + namespace + ["apply"] + cm_values
        result = subprocess.run(apply_cmd, stdout=sys.stdout)

    def run(self):

        self.configure()

        cmd = self.get_variable("HELM_COMMAND")
        key = cmd.split()[0]

        if key == "install" or key == "upgrade" or key == "template":
            self.handle_deploy(cmd)
        else:
            self.handle_generic(cmd)

        configmaps = self.get_variable("HELM_COPY_CONFIG_MAP")
        if configmaps:
            configmaps = configmaps.split(",")
            for configmap in configmaps:
                self.copy_configs("configmap", configmap)

        secrets = self.get_variable("HELM_COPY_SECRET")
        if secrets:
            secrets = secrets.split(",")
            for secret in secrets:
                self.copy_configs("secret", secret)

        self.success(message=f"Pipe finished successfully!")


if __name__ == "__main__":
    pipe = EKSHelmPipe(
        schema=schema, pipe_metadata_file="/pipe.yml", check_for_newer_version=True
    )
    pipe.run()
