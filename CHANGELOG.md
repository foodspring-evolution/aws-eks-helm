# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.0

- major: Locked helm version

## 0.5.6

- patch: stop using deprecated --export flag for kubectl

## 0.5.5

- patch: Fixed bad handling of generic secrets

## 0.5.4

- patch: Implemented multi secret and configmap copy

## 0.5.3

- patch: Fixed pipelines to use older packager

## 0.5.2

- patch: Fixed pipelines to push back to repo
- patch: Switched repository

## 0.5.1

- patch: Recreated semversioner directories

## 0.5.0

- minor: Added support for multiple configmaps
- minor: Multiple configmaps are separated by ","

## 0.4.1

- patch: Reversed namespace and copy_namespace

## 0.4.0

- minor: Added an option to copy configmap and secret
- minor: Copied secret is now modified by business logic

## 0.3.0

- minor: Reworked deploy to take values.yaml from configmap

## 0.2.0

- minor: Removed --wait from features

## 0.1.0

- minor: Implemented helm deploy functionality

