FROM bitbucketpipelines/kubectl-run:3.1.0

RUN apt-get update && apt-get install -y git curl
RUN curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash -s -- --version "v3.6.3"

COPY requirements.txt /

RUN pip install -r requirements.txt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python", "/pipe.py"]
